// Task 1
console.log('Request data...');
const promise = new Promise((resolve, reject) => {
        setTimeout(() => {
                console.log("Preparing data...");
        const data = {
                server: "aws",
                port: 2000,
                status: "pending"
            }
            resolve(data)
        }, 2000)
    })
    promise.then(data => {
    setTimeout(() => {
            data.status = "success"
            console.log("Data received ", data);
        }, 2000)
    }).catch(err => console.log(err))


// Task 2
const getData = async() => {
    const request = await fetch('https://jsonplaceholder.typicode.com/users')
    const response = await request.json()
    getResult(response)
    console.log(response);
}
getData()

function getResult(data) {
    const tBody = document.querySelector('tbody')
    data.forEach(item => {
        const tr = document.createElement('tr')
        console.log(item);
        tr.innerHTML = `
            <td>${item.name}</td>
            <td>${item.phone}</td>
            <td><a href=${item.website}>website</a></td>
        `
        tBody.append(tr)
    })
}